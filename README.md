    

# escaper
Der „escaper“ ersetzt von einer Python-Quelltextdatei die Zeilenschaltungen, Hochkomma, Tabulatoren, Backslashes. Damit kann der modifizierte Quelltext direkt in die REPL von MicroPython (bevorzugt via [strg]+[e]) einzufügen um die Inhalt gleich in eine Datei zu speichern.

| Option | long-opt | Description |
| -------- | -------- | -------- |
|-h	|–help	|shows this help|
|-l	|–licence	|shows licence (GPLv2)|
|-L	|–lizenz	|shows german translation of GPLv2|
|-c file	|–code=file	|code file|
|-o file	|–out=file	|write to file (else write to std out)|
|-m file	|–mp=file	|create code directly for (micropython) REPL|
|-r      | --reduce |             try to reduce code *experimental*|
|-i      | --import |             try to import neccessary files *experimental*|
| -c file | --code=file|           code file|
| -o file | --out=file|            write to file (else write to std out)|
| -m file | --mp=file |            create code directly for (micropython) REPL|
|     |                |            file is the filename on the target-system|
|     |                |             like this example:|
|    |                 |              fhandle = open(<file>,"w")|
|    |                 |               fhandle.write(<code>)|
|   |                  |               fhandle.close() |
| -d dev  | --dev=dev |            write directly to dev  (e.g. /dev/ttyUSB0)|
| -b baud | --baud=baud |          set the baudrate for communication with --dev|
|         |             |            default baudrate for micropython is 115200|
| -x      | --reset     |          reset the device \_\_import\_\_('machine').reset()|
| -g file | --get=file  |          read file from micropython-devices|
|         | --read=file |          alias for --get|
| -u      | --upload=file |        upload file (or directory) in "binary" mod|
## Install
Download [escaper_0.6_1FE.tgz](../src/branch/master/escaper_0.6_1FE.tgz) and extract it. 
```
wget  https://codeberg.org/butzel/escaper/src/commit/bbb23cb198e1b2d304a0f82894b05a16ab8f15c0/escaper_0.6_1FE.tgz
tar xf escaper_0.6_1FE.tgz
./escaper.py -h 
```
